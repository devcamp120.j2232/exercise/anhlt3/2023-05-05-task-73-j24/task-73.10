'use strict';
let gId = 0;
let gPrize = {
  prize: '',
  onGetPrizeClick() {
    $.ajax({
      url: 'http://localhost:8080/prizes',
      method: 'GET',
      dataType: 'json',
      success: getAllPrize,
      error: (err) => alert(err.responseText),
    });
  },
  onEditPrizeClick() {
    let vSelectedRow = $(this).parents('tr');
    let vSelectedData = gPrizeTable.row(vSelectedRow).data();
    gId = vSelectedData.id;
    $.ajax({
      url: `http://localhost:8080/prizes/${gId}`,
      method: 'GET',
      dataType: 'json',
      success: showPrizeDetail,
      error: (err) => alert(err.responseText),
    });
  },
  onCreatePrizeClick() {
    this.prize = {
      prizeCode: $('#input-prize-code').val().trim(),
      prizeName: $('#input-prize-name').val().trim()
    };
    if (validatePrize(this.prize)) {
      $.ajax({
        url: 'http://localhost:8080/prizes',
        method: 'POST',
        data: JSON.stringify(this.prize),
        contentType: 'application/json;charset=utf-8',
        success: () => {
          alert('Đã tạo prize thành công');
          gPrize.onGetPrizeClick();
          resetInput();
        },
        error: (err) => alert(err.responseText),
      });
    }
  },
  onSavePrizeClick() {
    this.prize = {
      prizeCode: $('#input-prize-code').val().trim(),
      prizeName: $('#input-prize-name').val().trim()
    };
    if (validatePrize(this.prize)) {
      $.ajax({
        url: `http://localhost:8080/prizes/${gId}`,
        method: 'PUT',
        data: JSON.stringify(this.prize),
        contentType: 'application/json;charset=utf-8',
        success: () => {
          alert('Đã cập nhật prize thành công');
          gPrize.onGetPrizeClick();
          resetInput();
        },
        error: (err) => alert(err.responseText),
      });
    }
  },
  onDeletePrizeClick() {
    $('#modal-delete-prize').modal('show');
    let vSelectedRow = $(this).parents('tr');
    let vSelectedData = gPrizeTable.row(vSelectedRow).data();
    $('#text-delete-prize').text('Bạn có chắc muốn xóa prize?');
    gId = vSelectedData.id;
  },
  onDeleteAllPrizesClick() {
    $('#modal-delete-prize').modal('show');
    gId = 0;
    $('#text-delete-prize').text('Bạn có chắc muốn xóa tất cả prize?');
  },
  onConfirmDeletePrizeClick() {
    if (gId === 0) {
      $.ajax({
        url: `http://localhost:8080/prizes`,
        type: 'DELETE',
        success: () => {
          alert('Đã xóa tất cả prize');
          $('#modal-delete-prize').modal('hide');
          gPrize.onGetPrizeClick();
        },
        error: (err) => alert(err.responseText),
      });
    } else {
      $.ajax({
        url: `http://localhost:8080/prizes/${gId}`,
        type: 'DELETE',
        success: () => {
          alert('Đã xóa prize thành công');
          $('#modal-delete-prize').modal('hide');
          gPrize.onGetPrizeClick();
        },
        error: (err) => alert(err.responseText),
      });
    }
  },
};
gPrize.onGetPrizeClick();

$('#prize-table').on('click', 'tr', gPrize.onGetPrizeClick);

$('#prize-table').on('click', '.fa-edit', gPrize.onEditPrizeClick);

$('#create-data').click(gPrize.onCreatePrizeClick);

$('#save_data').click(gPrize.onSavePrizeClick);

$('#prize-table').on('click', '.fa-trash', gPrize.onDeletePrizeClick);

$('#delete-prize').click(gPrize.onConfirmDeletePrizeClick);

$('#delete-all-prize').click(gPrize.onDeleteAllPrizesClick);

// tạo bảng prize bằng data Table
let gPrizeTable = $('#prize-table').DataTable({
  order: [],
  columns: [
    { data: 'id' },
    { data: 'prizeCode' },
    { data: 'prizeName' },
    { data: 'action' },
  ],
  columnDefs: [
    {
      targets: -1,
      defaultContent: `<i class="fas fa-edit text-primary"></i>
      | <i class="fas fa-trash text-danger"></i>`,
    },
  ],
});

// load voucher vào table
function loadPrizeToTable(paramPrize) {
  'use strict';
  gPrizeTable.clear().rows.add(paramPrize).draw();
}

// hàm hiển thị lên website
function getAllPrize(paramPrizes) {
  'use strict';
  loadPrizeToTable(paramPrizes);
}

function showPrizeDetail(paramPrize) {
  $('#input-prize-code').val(paramPrize.prizeCode);
  $('#input-prize-name').val(paramPrize.prizeName);
}

// hàm dùng để validate dữ liệu
function validatePrize(paramPrize) {
  'use strict';
  let vValidated = true;
  try {
    if (paramPrize.prizeCode == '') {
      vValidated = false;
      throw '100, Chưa nhập mã Prize';
    }
    if (paramPrize.prizeName == '') {
      vValidated = false;
      throw '200, Chưa nhập tên Prize';
    }
  } catch (err) {
    alert('Error: ' + err);
  }
  return vValidated;
}

// hàm dùng để reset input
function resetInput() {
  $('#input-prize-code').val('');
  $('#input-prize-name').val('');
}
