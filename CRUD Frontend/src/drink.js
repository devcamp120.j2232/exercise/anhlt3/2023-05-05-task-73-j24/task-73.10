'use strict';
let gDrinkId = 0;
let drink = {
  newDrink: {
    maNuocUong: '',
    tenNuocUong: '',
    donGia: '',
    ghiChu: '',
  },
  onCreateNewDrinkClick() {
    this.newDrink = {
      maNuocUong: $('#input-drink-code').val().trim(),
      tenNuocUong: $('#input-drink-name').val(),
      donGia: $('#input-drink-price').val().trim(),
      ghiChu: $('#input-note').val().trim(),
    };
    if (validateDrink(this.newDrink)) {
      $.ajax({
        url: 'http://localhost:8080/drinks',
        method: 'POST',
        data: JSON.stringify(this.newDrink),
        contentType: 'application/json',
        success: (data) => {
          alert('Drink created successfully');
          getDrinkFromDb();
          resetDrinkInput();
        },
        error: (err) => alert(err.responseText),
      });
    }
  },
  onUpdateDrinkClick() {
    let vSelectedRow = $(this).parents('tr');
    let vSelectedData = drinkTable.row(vSelectedRow).data();
    gDrinkId = vSelectedData.id;
    $.get(`http://localhost:8080/drinks/${gDrinkId}`, loadDrinkToInput);
  },
  onSaveDrinkClick() {
    this.newDrink = {
      maNuocUong: $('#input-drink-code').val().trim(),
      tenNuocUong: $('#input-drink-name').val().trim(),
      donGia: $('#input-drink-price').val().trim(),
      ghiChu: $('#input-note').val().trim(),
    };
    if (validateDrink(this.newDrink)) {
      $.ajax({
        url: `http://localhost:8080/drinks/${gDrinkId}`,
        method: 'PUT',
        data: JSON.stringify(this.newDrink),
        contentType: 'application/json',
        success: (data) => {
          alert('Drink updated successfully');
          getDrinkFromDb();
          gDrinkId = 0;
          resetDrinkInput();
        },
        error: (err) => alert(err.responseText),
      });
    }
  },
  onDeleteDrinkByIdClick() {
    $('#modal-delete-drink').modal('show');
    let vSelectedRow = $(this).parents('tr');
    let vSelectedData = drinkTable.row(vSelectedRow).data();
    gDrinkId = vSelectedData.id;
  },
  onDeleteAllDrinkClick() {
    $('#modal-delete-drink').modal('show');
    gDrinkId = 0;
  },
  onDeleteConfirmClick() {
    if (gDrinkId == 0) {
      $.ajax({
        url: 'http://localhost:8080/drinks',
        method: 'DELETE',
        success: () => {
          alert('All drink were successfully deleted');
          getDrinkFromDb();
          $('#modal-delete-drink').modal('hide');
        },
        error: (err) => alert(err.responseText),
      });
    } else {
      $.ajax({
        url: `http://localhost:8080/drinks/${gDrinkId}`,
        method: 'DELETE',
        success: () => {
          alert(`Drink with id: ${gDrinkId} was successfully deleted`);
          getDrinkFromDb();
          $('#modal-delete-drink').modal('hide');
        },
        error: (err) => alert(err.responseText),
      });
    }
  },
};

let drinkTable = $('#drink-table').DataTable({
  columns: [
    { data: 'id' },
    { data: 'maNuocUong' },
    { data: 'tenNuocUong' },
    { data: 'donGia' },
    { data: 'ghiChu' },
    { data: 'ngayTao' },
    { data: 'ngayCapNhat' },
    { data: 'action' },
  ],
  columnDefs: [
    {
      targets: -1,
      defaultContent: `<i class="fas fa-edit text-primary"></i>
      | <i class="fas fa-trash text-danger"></i>`,
    },
  ],
});

function loadDrinkOnTable(paramDrinks) {
  'use strict';
  drinkTable.clear();
  drinkTable.rows.add(paramDrinks);
  drinkTable.draw();
}

function getDrinkFromDb() {
  'use strict';
  $.get('http://localhost:8080/drinks', (drink) => loadDrinkOnTable(drink));
}
getDrinkFromDb();

$('#create-drink').click(drink.onCreateNewDrinkClick);
$('#drink-table').on('click', '.fa-edit', drink.onUpdateDrinkClick);
$('#drink-table').on('click', '.fa-trash', drink.onDeleteDrinkByIdClick);
$('#update-drink').click(drink.onSaveDrinkClick);
$('#delete-all-drink').click(drink.onDeleteAllDrinkClick);
$('#delete-drink').click(drink.onDeleteConfirmClick);

function loadDrinkToInput(paramDrinks) {
  $('#input-drink-code').val(paramDrinks.maNuocUong);
  $('#input-drink-name').val(paramDrinks.tenNuocUong);
  $('#input-drink-price').val(paramDrinks.donGia);
  $('#input-note').val(paramDrinks.ghiChu);
}

function resetDrinkInput() {
  $('#input-drink-code').val('');
  $('#input-drink-name').val('');
  $('#input-drink-price').val('');
  $('#input-note').val('');
}

function validateDrink(paramDrinks) {
  'use strict';
  let vResult = true;
  try {
    if (paramDrinks.maNuocUong == '') {
      vResult = false;
      throw '100.cần nhập mã nước uống';
    }
    if (paramDrinks.tenNuocUong == '') {
      vResult = false;
      throw '200.cần nhập tên nước uống';
    }
    if (
      isNaN(paramDrinks.donGia) ||
      paramDrinks.donGia < 0 ||
      paramDrinks.donGia == ''
    ) {
      vResult = false;
      throw 'cần nhập giá nước uống';
    }
  } catch (e) {
    alert(e);
  }
  return vResult;
}
