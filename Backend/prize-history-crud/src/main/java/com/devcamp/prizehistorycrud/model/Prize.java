package com.devcamp.prizehistorycrud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "prizes")
public class Prize {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull(message = "Nhập mã giải thưởng")
    @Size(min = 2, message = "Mã giải thưởng phải có ít nhất 2 ký tự ")
    @Column(name = "prize_code", unique = true)
    private String prizeCode;

    @NotEmpty(message = "Nhập tên giải thưởng")
    @Column(name = "prize_name")
    private String prizeName;

    public Prize() {
    }

    public Prize(long id, String prizeCode, String prizeName) {
        this.id = id;
        this.prizeCode = prizeCode;
        this.prizeName = prizeName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPrizeCode() {
        return prizeCode;
    }

    public void setPrizeCode(String prizeCode) {
        this.prizeCode = prizeCode;
    }

    public String getPrizeName() {
        return prizeName;
    }

    public void setPrizeName(String prizeName) {
        this.prizeName = prizeName;
    }

}
