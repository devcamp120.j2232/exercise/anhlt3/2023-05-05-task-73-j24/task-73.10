package com.devcamp.prizehistorycrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.prizehistorycrud.model.Prize;

public interface IPrizeRepository extends JpaRepository<Prize, Long> {
    
}
