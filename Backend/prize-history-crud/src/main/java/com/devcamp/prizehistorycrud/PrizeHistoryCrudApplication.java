package com.devcamp.prizehistorycrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrizeHistoryCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrizeHistoryCrudApplication.class, args);
	}

}
