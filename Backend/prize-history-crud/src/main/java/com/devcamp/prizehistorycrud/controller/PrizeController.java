package com.devcamp.prizehistorycrud.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.prizehistorycrud.model.Prize;
import com.devcamp.prizehistorycrud.repository.IPrizeRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class PrizeController {
    @Autowired
	IPrizeRepository pPrizeRepository;

	@GetMapping("/prizes")
	public ResponseEntity<List<Prize>> getAllPrizes() {
		try {
			List<Prize> pPrizes = new ArrayList<Prize>();
			pPrizeRepository.findAll().forEach(pPrizes::add);
			return new ResponseEntity<>(pPrizes, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/prizes/{id}")
	public ResponseEntity<Prize> getPrizeById(@PathVariable("id") long id) {
		Optional<Prize> prizeData = pPrizeRepository.findById(id);
		if (prizeData.isPresent()) {
			return new ResponseEntity<>(prizeData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/prizes")
	public ResponseEntity<Prize> createPrize(@Valid @RequestBody Prize pPrizes) {
		try {
			pPrizes.setPrizeName(pPrizes.getPrizeName());
			Prize _prizes = pPrizeRepository.save(pPrizes);
			return new ResponseEntity<>(_prizes, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/prizes/{id}")
	public ResponseEntity<Prize> updatePrizeById(@PathVariable("id") long id,
			@Valid @RequestBody Prize pPrizes) {
		try {
			Optional<Prize> prizeData = pPrizeRepository.findById(id);

			if (prizeData.isPresent()) {
				Prize prize = prizeData.get();
				prize.setPrizeName(pPrizes.getPrizeName());
				return new ResponseEntity<>(pPrizeRepository.save(prize), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/prizes/{id}")
	public ResponseEntity<Prize> deletePrizeById(@PathVariable("id") long id) {
		try {
			pPrizeRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/prizes")
	public ResponseEntity<HttpStatus> deleteAllPrizes() {
		try {
			pPrizeRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
