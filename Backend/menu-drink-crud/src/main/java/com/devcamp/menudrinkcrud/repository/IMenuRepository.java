package com.devcamp.menudrinkcrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.menudrinkcrud.model.Menu;

public interface IMenuRepository extends JpaRepository<Menu, Long> {

}
